<?php
class Controllers_Users extends RestController {
	private $_con;
	private function DBconnect(){
		$this->_con=mysqli_connect("localhost","rotemze","76153","rotemze_test");// user Cpanel, password, name of DB
	}
	public function get() {
		$this->DBconnect();
		//var_dump($this->request['params']['id']);
		//die();
		if (isset($this->request['params']['id'])){
			if(!empty($this->request['params']['id'])) {
				$sql="SELECT * FROM users WHERE id=?";				
				$stmt = $this->_con->prepare($sql);
				$stmt->bind_param("s",$this->request['params']['id']);
				$stmt->execute();
				$stmt->store_result();				
				$stmt->bind_result($id, $name,$email);
				$stmt->fetch();
				$result = ['id'=>$id,'name'=>$name,'email'=>$email];
				$this->response = array('result' =>$result );
				$this->responseStatus = 200;
			} else {
				$sql="SELECT * FROM users";
				$usersSqlResult=mysqli_query($this->_con,$sql);
				$users=[];
				while ($user=mysqli_fetch_array($usersSqlResult)){
					$users[]=['name'=>$user['name'],'email'=>$user['email']];
				}
				$this->response=array('result'=>$users);
			}	
		}else{
			$this->response = array('result' =>'Wrong parameters for users' );
			$this->responseStatus = 200;			
		}
	}
	public function post() {
			$this->DBconnect();
			//the post parameter named as payload:{"name":"rotem","email":"roetm@rotem.com"}
				if(!empty($this->request['params']['payload'])) {
					$user []= json_decode($this->request['params']['payload'],true);
					var_dump("the payload from POST is: ".$this->request['params']['payload']);
					$name=$user[0]['name'];
					$email=$user[0]['email'];
					var_dump("the name is:".$name);
					var_dump("the email is:".$email);
					$sql=mysqli_query($this->_con,"INSERT INTO `users` (`name`, `email`) VALUES ('$name','$email')");
					$sql2="SELECT * FROM `users` ORDER BY `id` DESC LIMIT 1";
					$result = mysqli_query($this->_con, $sql2);
					if (mysqli_num_rows($result) > 0) {
						while($row = mysqli_fetch_array($result)) {
							$this->response=array("Last ID is: ".$row['id']);
						}
					}
				}
				else{
					$this->response = array('result' => 'no post implemented for users');
					$this->responseStatus = 201;
				}
		}
	public function put() {
		$this->response = array('result' => 'no put implemented for users');
		$this->responseStatus = 200;
	}
	public function delete() {
		$this->response = array('result' => 'no delete implemented for users');
		$this->responseStatus = 200;
	}
}
