import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import {AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './users/posts/posts.component';
import { PostsService } from './users/posts/posts.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserComponent } from './user/user.component';
import { UsersService } from './users/users.service';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoicesService } from './invoices/invoices.service';
import { InvoiceComponent } from './invoices/invoice/invoice.component';

const firebaseConfig = {
   apiKey: "AIzaSyBAyU2maEJJ5V7Sj2wjqZS0L0X6vvTqwV8",
    authDomain: "angular-9bd56.firebaseapp.com",
    databaseURL: "https://angular-9bd56.firebaseio.com",
    storageBucket: "angular-9bd56.appspot.com",
    messagingSenderId: "491021864527"
}

const appRoutes:Routes =[
  {path:'invoice-form',component:InvoiceFormComponent},
  {path:'invoices',component:InvoicesComponent},
  {path:'',component:InvoiceFormComponent},
  {path:'**',component:PageNotFoundComponent},
] 

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserComponent,
    UserFormComponent,
    PostFormComponent,
    InvoiceFormComponent,
    InvoicesComponent,
    InvoiceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [PostsService , UsersService, InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
