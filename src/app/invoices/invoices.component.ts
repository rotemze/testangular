import { Component, OnInit } from '@angular/core';
import { InvoicesService } from './invoices.service';

@Component({
  selector: 'jce-invoices',
  templateUrl: './invoices.component.html',
  styles: [`
       .invoices li { cursor: default; }
       .invoices li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class InvoicesComponent implements OnInit {

  currentinvoice;

  invoices;
  isLoading:Boolean = true;

  select(invoice){
    this.currentinvoice = invoice;
  }

  addInvoice(invoice){
    this._invoiceService.addInvoice(invoice);

    // this._invoiceService.addInvoice(invoice);
  }

  constructor(private _invoiceService:InvoicesService) { }

  ngOnInit() {
    this._invoiceService.getInvoices().subscribe(invoicesDate =>
    {this.invoices = invoicesDate; this.isLoading = false});
  }

}
