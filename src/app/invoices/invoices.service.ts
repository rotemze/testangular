import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesService {

  invoicesObservable;


  addInvoice(invoice){
    this.invoicesObservable.push(invoice);
  }


  // addInvoice(invoice){
  //   this.invoicesObservable.push(invoice);
  // }

  getInvoices(){
    // return this._http.get(this._url).map(res =>res.json()).delay(2000)
    return this.invoicesObservable = this.af.database.list('/invoices').delay(2000);
  }
  

  constructor(private af:AngularFire) { }

}
