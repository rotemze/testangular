import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styles: [`
    .posts li { cursor: default; }
    .posts li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }  
    }     
  `]
})
export class PostsComponent implements OnInit {
  
  isLoading:Boolean = true;

  posts;

  currentPost;

  select(post){
    this.currentPost = post;
  }

  constructor(private _postService: PostsService) { }
  deletePost(post){
    this._postService.deletePost(post); 
  }
  addPost(post){
    this._postService.addPost(post);
  }
  updatePost(post){
    this._postService.updatePost(post);
   }

  ngOnInit() {
    this._postService.getPosts().subscribe(postsData => 
    {this.posts = postsData; this.isLoading = false;
      console.log(this.posts)});
  }

}
